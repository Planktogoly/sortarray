package net.planckton.app;

public class App {
	
	public static void main(String[] args) {
		double[] array = new double[]{20.0, 16.5, 23.2, 0.1, 4.0};
		
		for (double item : array) {
			System.out.println(item);
		}
		
	    int arrayLength = array.length;
	    double temp = 0;

	    for (int i = 0; i < arrayLength; i++) {
	        for (int j = 1; j < (arrayLength - i); j++) {

	            if (array[j - 1] > array[j]) {
	                temp = array[j - 1];
	                array[j - 1] = array[j];
	                array[j] = temp;
	            }

	        }
	    }
	    
	    System.out.println(" ");
		for (double item : array) {
			System.out.println(item);
		}
	}

}
